#include <iostream>
#include<vector>
#include<cmath>
#include<algorithm>
#define PI 3.14159265
using namespace std;

class Punkt
{
public:


    double szerstpmin;
    double dlustpmin;
    string kierunekszerokosci;
    string kierunekdlugosc;
    int szerstopnie;
    int szerminuty;
    int dlstopnie;
    int dlminuty;

    string kod;
    Punkt(string kod="",string kierunekszerokosci="",string kierunekdlugosc="", int szerstopnie=0, int szerminuty=0, int dlstopnie=0, int dlminuty=0,double szerstpmin=0,double dlustpmin=0)
    {
        this->kod=kod;
        this->kierunekdlugosc=kierunekdlugosc;
        this->kierunekszerokosci=kierunekszerokosci;
        this->szerstopnie=szerstopnie;
        this->szerminuty=szerminuty;
        this->dlstopnie=dlstopnie;
        this->dlminuty=dlminuty;
    }

};
class SortVector
{
    public:
    vector<Punkt> punkty;
    int partition(vector<Punkt>& punkt, int p, int r)
    {
        Punkt w;
        int i = p, j = r,k;
        Punkt x = punkt[p];

        while (true)
        {
            while (punkt[j].dlustpmin < x.dlustpmin)
                j--;
            while (punkt[i].dlustpmin > x.dlustpmin )
                i++;
            if (i < j)
            {
                k=j;
                w = punkt[i];
                punkt[i] = punkt[j];
                k--;
                punkt[j] = w;
                i++;
                j--;
            }
            else
                return j;
        }
    }
    void quicksort(vector<Punkt>& punkt, int p, int r)
    {
        int q,l;
        if (p < r)
        {
            q = partition(punkt,p,r);
            l=r;
            quicksort(punkt, p, q);
            l=p;
            quicksort(punkt, q+1, r);
        }
    }

};
double licz_ortodromy(Punkt w1,Punkt w2)
{
    long double sin1sin2=0;
    long double cos1cos2=0;
    long double wynik=0;
    long double tmp1,tmp2,tmp1dlugosc,tmp2dlugosc;

    tmp1=(double)w1.szerminuty/60;
    tmp2=(double)w2.szerminuty/60;
    tmp1dlugosc=(double)w1.dlminuty/60;
    tmp2dlugosc=(double)w2.dlminuty/60;
    double fi1=w1.szerstopnie+tmp1;
    fi1=fi1*PI/180;
    double fi2=w2.szerstopnie+tmp2;
    fi2=fi2*PI/180;
    double tmp1rad=w1.dlstopnie+tmp1dlugosc;
    tmp1rad=tmp1rad*PI/180;
    double tmp2rad=w2.dlstopnie+tmp2dlugosc;
    tmp2rad=tmp2rad*PI/180;
    double f1minusf2szerokosc=fi1-fi2;
    double f1minusf2dlugosc=abs((tmp1rad)-(tmp2rad));
    return acos((sin(fi1)*sin(fi2))+(cos(fi1)*cos(fi2)*cos(f1minusf2dlugosc)));

}


int main()
{
    SortVector v1;
    int ilosc;
    cin>>ilosc;
    long double sfera;
    cin>>sfera;
    long double b=sfera/(2*PI);
    Punkt p1;
    for(int i=0; i<ilosc; i++)
    {
        Punkt p1;
        string kod;
        string kierunek;
        int szerstopnie;
        int szerminuty;
        int dlstopnie;
        int dlminuty;
        cin>>p1.kod>>p1.szerstopnie>>p1.szerminuty>>p1.kierunekszerokosci>>p1.dlstopnie>>p1.dlminuty>>p1.kierunekdlugosc; // zmiana znaku przy poludniowych i zachodnim
        double tmp1,tmp2;
        tmp1=(double)p1.szerstopnie;
        tmp2=(double)p1.dlstopnie;
        p1.dlustpmin=tmp2+(double)p1.dlminuty/60;
        p1.szerstpmin=tmp1+(double)p1.szerminuty/60;


        if(p1.kierunekszerokosci=="S")
        {
            p1.szerstopnie=p1.szerstopnie*(-1);
            p1.szerstpmin=p1.szerstpmin*(-1);
        }
        if(p1.kierunekdlugosc=="W")
        {
            p1.dlstopnie=p1.dlstopnie*(-1);
            p1.dlustpmin=p1.dlustpmin*(-1);
        }
        v1.punkty.push_back(p1);

    }
    Punkt p1min,p2min;
    v1.quicksort(v1.punkty,0,v1.punkty.size()-1);
    for(int i = 0 ; i < v1.punkty.size()-1; i++)
    {
        if(v1.punkty[i].dlustpmin == v1.punkty[i+1].dlustpmin)
            if(v1.punkty[i+1].szerstpmin < v1.punkty[i].szerstpmin)
            {
                swap(v1.punkty[i+1],v1.punkty[i]);
            }
    }

    double min = licz_ortodromy(v1.punkty[0],v1.punkty[1]);
    p1min=v1.punkty[0];
    p2min=v1.punkty[1];
    for(int i = 1 ; i < v1.punkty.size()-1; i++)
    {
        double tmp = licz_ortodromy(v1.punkty[i],v1.punkty[i+1]);
        if(tmp<=min)
        {
            min=tmp;
            p1min=v1.punkty[i];
            p2min=v1.punkty[i+1];

        }

    }
    vector<string>kody;
    kody.push_back(p1min.kod);
    kody.push_back(p2min.kod);
    sort(kody.begin(),kody.end());
    cout<<kody[0]<<" "<<kody[1];

    return 0;
}
